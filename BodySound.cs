﻿using UnityEngine;
using System.Collections;
using MacGruber;

public class BodySound : MVRScript
{

    JSONStorableActionAudioClip actionAudioClip;

    public override void Init()
    {

        Utils.SetupAction(this, "自定义的触发目标", () =>
        {
            //在这里可以执行我们想要的操作。
            SuperController.singleton.Message("触发了自定义的目标");
        });

    }
}
