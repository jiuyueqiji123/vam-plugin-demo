using UnityEngine;
using System.Collections;
using MacGruber;
using System;
using System.Linq;
using System.Collections.Generic;
using MeshVR;

// 变形器的操作
public class DeformerDemo : MVRScript
{
    private DAZMorph leftHandFist;
    private DAZMorph rightHandFist;

    public override void Init()
    {
        base.Init();

        JSONStorable storableByID = this.containingAtom.GetStorableByID("geometry");
        if (storableByID != null)
        {
            // 查找左右手相关的变形器
            DAZCharacterSelector dazcharacterSelector = storableByID as DAZCharacterSelector;
            GenerateDAZMorphsControlUI morphsControlUI = dazcharacterSelector.morphsControlUI;
            if (morphsControlUI != null)
            {
                // region代表的变形的类别，morphName代表实际的变形名称
                List<DAZMorph> morphs = morphsControlUI.GetMorphs();
                morphs.Where(x => x.region.StartsWith("Pose Controls/Hands/Left")).ToList().ForEach(x => SuperController.LogMessage(x.morphName));
                this.leftHandFist = morphs.FirstOrDefault(x => x.region == "Pose Controls/Hands/Left" && x.morphName == "CTRLlHandFist");
                this.rightHandFist = morphs.FirstOrDefault(x => x.region == "Pose Controls/Hands/Right" && x.morphName == "CTRLrHandFist");

                // 初始化CTRLlHandFist变形器
                if (this.leftHandFist != null)
                {
                    this.leftHandFist.SetValue(0f);
                    this.leftHandFist.isTransient = false;
                    this.leftHandFist.isRuntime = false;
                }
                else
                {
                    SuperController.LogMessage("LeftHand CTRLlHandFist not found");
                }

                // 初始化CTRLrHandFist变形器
                if (this.rightHandFist != null)
                {
                    this.rightHandFist.SetValue(0f);
                    this.rightHandFist.isTransient = false;
                    this.rightHandFist.isRuntime = false;
                }
                else
                {
                    SuperController.LogMessage("RightHand CTRLlHandFist not found");
                }
            }
        }
    }

    void Update()
    {
        // 左手按下手柄握持键时，设置手部变形
        if (this.leftHandFist != null)
        {
            this.leftHandFist.SetValue(GetLeftTriggerVal());
        }

        // 右手按下手柄握持键时，设置手部变形
        if (this.rightHandFist != null)
        {
            this.rightHandFist.SetValue(GetRightTriggerVal());
        }
    }
}