﻿using UnityEngine;
using System.Collections;

public class PingpongGameManager 
{

    public PingpongGame pingpongGame;

    public JSONStorableFloat ballStayDuration;

    public int gameChieventState;

    private static PingpongGameManager _instance;

    public static PingpongGameManager singleton {
        get {
            if (_instance == null) _instance = new PingpongGameManager();
            return _instance;
        }
    }

}
