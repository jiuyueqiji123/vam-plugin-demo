﻿using UnityEngine;
using System.Collections;
using MacGruber;
using System.Linq;

public class DebugTool : MVRScript
{

    public override void Init()
    {
        string logStr = "";

        Utils.SetupButton(this, "打印所有控制体", () => {
            SuperController.singleton.ClearMessages();
            var controllerList = GetAllControllers();
            logStr = "";
            for (int i = 0; i < controllerList.Length; i++)
            {
                logStr += controllerList[i].name + "\n";
            }
            SuperController.LogMessage(logStr);
        }, false);

        Utils.SetupButton(this, "打印所有原子", () => {
            SuperController.singleton.ClearMessages();
            var atomList = GetAtomUIDs();
            logStr = "";
            for (int i = 0; i < atomList.Count; i++)
            {
                logStr += atomList[i] + "\n";
            }
            SuperController.LogMessage(logStr);
        }, false);

        Utils.SetupSpacer(this, 10, false);

        Utils.SetupButton(this, "打印当前所选物体的层次结构(简要)", () => {
            var atom = SuperController.singleton.GetSelectedAtom();
            if (atom == null) return;
            SuperController.singleton.ClearMessages();
            Utils.LogTransform(string.Format("{0}原子的结构", atom.name), atom.transform);
        }, false);

        Utils.SetupButton(this, "打印当前所选物体的层次结构(详细)", () => {
            var atom = SuperController.singleton.GetSelectedAtom();
            if (atom == null) return;
            SuperController.singleton.ClearMessages();
            Utils.LogTransform(string.Format("{0}原子的结构", atom.name), atom.transform,false);
        }, false);

    }
}
