﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{

    Collider collider;
    Rigidbody rgd;
    float startTime;

    void Start()
    {
        collider = GetComponent<Collider>();
        rgd = GetComponent<Rigidbody>();
        rgd.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        collider.material = new PhysicMaterial("ball") { bounciness = 1f };
    }

    private void OnEnable()
    {
        PingpongGameManager.singleton.ballStayDuration.val = 0;
        startTime = Time.time;
        rgd.velocity = Vector3.zero;
    }

    void Update()
    {
        float stayedTime = Time.time - startTime;
        PingpongGameManager.singleton.ballStayDuration.val = stayedTime;

        if (transform.position.y < -1)
        {
            gameObject.SetActive(false);
        }
    }
}
