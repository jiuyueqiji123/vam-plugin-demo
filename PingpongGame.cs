﻿using UnityEngine;
using UnityEngine.XR;
using System;
using MacGruber;

public class PingpongGame : MVRScript
{

    private JSONStorableFloat ballRadius;
    private JSONStorableColor ballColor;
    private JSONStorableAction chievementAction;

    private Transform lo, ro;
    private bool gameIsVR;

    private GameObject ball;

    private EventTrigger achieventTrigger;
    private FloatTrigger stayDurationTrigger;

    public override void Init()
    {

        PingpongGameManager.singleton.gameChieventState = 0;
        PingpongGameManager.singleton.pingpongGame = this;

        try
        {
            gameIsVR = XRSettings.isDeviceActive && !string.IsNullOrEmpty(XRSettings.loadedDeviceName);
            SuperController.LogMessage("PingpongGameLoaded");
            if (gameIsVR)
            {
                //左手的物体与右手物体
                ro = SuperController.singleton.rightHand;
                lo = SuperController.singleton.leftHand;
            }

            ballRadius = Utils.SetupSliderFloat(this, "Radius", 0.05f, 0.01f, 0.2f, false);
            PingpongGameManager.singleton.ballStayDuration = Utils.SetupSliderInt(this, "球保持时间", 0, 0, 1000, false);
            ballColor = Utils.SetupColor(this, "Bullet Color", Color.gray, false);

            //加载UI资源
            SimpleTriggerHandler.LoadAssets();

            //成就触发器
            achieventTrigger = new EventTrigger(this, "成就触发事件");
            Utils.SetupButton(this, "设置成就触发", achieventTrigger.OpenPanel, false);
            SuperController.singleton.onAtomUIDRenameHandlers += OnAtomRenamed;

            //分数触发器
            stayDurationTrigger = new FloatTrigger(this, "分数触发");
            Utils.SetupButton(this, "设置分数触发", stayDurationTrigger.OpenPanel, false);
            SuperController.singleton.onAtomUIDRenameHandlers += OnAtomRenamed;
        }
        catch (Exception e)
        {
            SuperController.LogError("初始化错误啦" + e.Message);
            throw;
        }
    }

    bool isCreated;
    private void Update()
    {
        //生成球的逻辑
        if (lo != null)
        {
            if (GetLeftTriggerVal() > 0.3f)
            {
                if (!isCreated)
                {
                    isCreated = true;
                    CreatePingpongBall();
                }
            }
            else
            {
                isCreated = false;
            }
        }

        //段位判定
        var stayDur = PingpongGameManager.singleton.ballStayDuration.val;
        if (PingpongGameManager.singleton.gameChieventState == 0 && stayDur > 5)
        {
            PingpongGameManager.singleton.gameChieventState = 1;
            achieventTrigger.Trigger();
            SuperController.LogMessage("不错哦！");
        }
        else if (PingpongGameManager.singleton.gameChieventState == 1 && stayDur > 15)
        {
            PingpongGameManager.singleton.gameChieventState = 2;
            achieventTrigger.Trigger();
            SuperController.LogMessage("加油！");
        }
        else if (PingpongGameManager.singleton.gameChieventState == 2 && stayDur > 30)
        {
            PingpongGameManager.singleton.gameChieventState = 3;
            achieventTrigger.Trigger();
            SuperController.LogMessage("牛逼！");
        }
        else if (PingpongGameManager.singleton.gameChieventState == 3 && stayDur > 60)
        {
            PingpongGameManager.singleton.gameChieventState = 4;
            achieventTrigger.Trigger();
            SuperController.LogMessage("吊炸天！");
        }
        else if (PingpongGameManager.singleton.gameChieventState == 4 && stayDur > 120)
        {
            PingpongGameManager.singleton.gameChieventState = 5;
            achieventTrigger.Trigger();
            SuperController.LogMessage("超神！");
        }
        else if (PingpongGameManager.singleton.gameChieventState == 5 && stayDur > 240)
        {
            PingpongGameManager.singleton.gameChieventState = 6;
            achieventTrigger.Trigger();
            SuperController.LogMessage("你确定你是地球人？");
        }

        //更新触发器的监听
        achieventTrigger.Update();
        stayDurationTrigger.Update();
    }

    private void CreatePingpongBall()
    {
        PingpongGameManager.singleton.gameChieventState = 0;

        if (ball == null)
        {
            ball = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            ball.AddComponent<SphereCollider>();
            ball.AddComponent<Ball>();
            ball.AddComponent<Rigidbody>();
        }
        else
        {
            ball.SetActive(true);
        }

        ball.transform.localScale = Vector3.one * ballRadius.val;
        ball.GetComponent<MeshRenderer>().material.color = HSVColorPicker.HSVToRGB(ballColor.val);
        ball.transform.position = lo.transform.position;
    }

    private void OnAchieventTrigger()
    {
        SuperController.LogMessage("触发了");
        achieventTrigger.Trigger();
    }

    private void OnStayDurationTrigger(float val)
    {
        stayDurationTrigger.Trigger(val);
    }

    private void OnAtomRenamed(string oldName, string newName)
    {
        achieventTrigger.SyncAtomNames();
        stayDurationTrigger.SyncAtomNames();
    }

    private void OnDestroy()
    {
        SuperController.singleton.onAtomUIDRenameHandlers -= OnAtomRenamed;
        achieventTrigger.Remove();
        stayDurationTrigger.Remove();
    }

}

